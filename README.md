# vtt_flutter

A new VTT Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

### How to create
Untuk membuat aplikasi speect to text yang sederhana dapat ikuti langkah-langkah berikut ini:
1. tambahkan packages speect_to_text pada pubspec.yaml
```yaml
...
dependencies:
  speech_to_text: ^latest_version
...
```
setelah melakukan perubahan pada pubspec.yaml pastikan jalankan pub get pada IDE/terminal anda
```bash
$ flutter pub get
```
2. masukkan beberapa permission pada AndroidManifest.xml
Anda bisa salin dari kode berikut:
```xml
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.INTERNET"/>
    <queries>
        <intent>
            <action android:name="android.speech.RecognitionService" />
        </intent>
    </queries> 
```

3. setelah itu lakukan inisiasi variabel pada *class* vtt yang ingin digunakan
```dart
    // variabel ini untuk memanggil kelas 
    // SpeechToText agar kita bisa menggunakan 
    // fungsi didalam *class* tersebut 
    final _speechToText = SpeechToText();
    // variabel ini digunakan untuk mengontrol 
    // teks pada textfield ataupun textformfield
    final speechController = TextEditingController(); 
    
    // variabel ini untuk pengecekan apakah 
    // *class* SpeechToText() yang telah di panggil 
    // tadi sudah terinisiasi ataukah belum.
    bool _speechEnabled = false;
    // variabel ini untuk menyimpan data ketika voice 
    // diaktifkan.
    String _lastWords = '';
```

4. Buat fungsi untuk kembalian seperti berikut
```dart
  void _onSpeechResult(SpeechRecognitionResult result) {
    setState(() {
      // disini _lastWords diisi dengan data 
      // _lastWords sebelumnya ditambah dengan 
      // data baru dari recognizedWords
      _lastWords = '$_lastWords ${result.recognizedWords}';
      
      // setelah itu data _lastWords di 
      // masukkan pada text controller yang telah 
      // dibuat tadi.
      speechController.text = _lastWords;
    });
  }
```

5. Setelah itu buat fungsi untuk memanggil inisiasi dari fungsi *class* SpeechToText() dan kemudian panggil fungsi tersebut pada fungsi initState
```dart
  void _initSpeech() async {
    // isi variabel _speechEnabled tadi 
    // dengan hasil dari inisiasi speechToText.  
    _speechEnabled = await _speechToText.initialize();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    // kemudian panggil fungsi _initSpeech() 
    // pada initState agar fungsi tersebut dijalankan 
    // ketika *class* pertama kali dibuka.
    _initSpeech();
  }
```

6. Setelah semua fungsi inisiasi selesai di buat, langkah selanjutnya adalah membuat fungsi untuk start dan stop perekaman suaranya.
Anda bisa membuat dua fungsi sederhana seperti berikut

```dart
  void _startListening() async {
    await _speechToText.listen(
      onResult: _onSpeechResult,
      localeId: 'en_En',
      listenFor: const Duration(seconds: 30),
    );
    setState(() {});
  }

  void _stopListening() async {
    await _speechToText.stop();
    setState(() {});
  }
```

Untuk menggunakan fungsi-fungsi tersebut, anda bisa gunakan referensi dari contoh berikut
```dart
// fungsi ini digunakan untuk mengetahui apakah 
// fungsi _speechToText ini sedang dijalankan ataukah tidak
if (_speechToText.isListening) {
  // apabila sedang dijalankan maka tombol jika ditekan 
  // akan melakukan stop 
  _stopListening();
} else {
  // jika tidak, 
  // disini terdapat pengecekan untuk mengetahui apakah 
  // layanan speechToText berhasil di inisiasi atau tidak
  if (_speechEnabled) {
    // apabila proses inisiasi berhasil
    // ketika tombol ditekan maka akan menjalankan start voice, 
    // kemudian anda bisa bicara dan akan otomatis menjadi teks
    _startListening();
  } else {
    
    // apabila proses inisiasi gagal
    // maka disini akan memunculkan snackbar dengan pesan
    // 'Speech not available!'
    const snackBar =
    SnackBar(content: Text('Speech not available!'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
```

Untuk menampilkan hasil dari suara yang di inputkan, bisa dengan membuat textfield sederhana seperti berikut:
```dart
TextField(
  // anda bisa menambahkan teks kontroler yang telah dibuat tadi pada textfield.
  controller: speechController,
  readOnly: true,
    decoration: InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: const BorderSide(
      color: Colors.redAccent,
      ),
    ),
  ),
),
```
